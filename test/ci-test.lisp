#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

#+sbcl (setf sb-ext:*derive-function-types* t)

(ql:register-local-projects)
(ql:update-dist "quicklisp" :prompt nil)
(ql:quickload :moppack.test)
(moppack.test:ci-run)
