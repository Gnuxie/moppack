#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:storage-test.test)

(parachute:define-test instance-pointer-simple

  (storage-test::with-instance-pointer-handlers* (table-id 4)
      (page-number 8)
      (page-index 2)
      write-instance-pointer
    (let ((pointer (write-instance-pointer 12 3 1)))
      (parachute:is #'= 12 (table-id pointer))
      (parachute:is #'= 3 (page-number pointer))
      (parachute:is #'= 1 (page-index pointer)))))
