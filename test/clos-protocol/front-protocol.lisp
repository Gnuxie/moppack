#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.test.clos-protocol)

(defclass test-bodge ()
  ((a :initarg :a
      :type string
      :reader a)

   (b :initarg :b
      :type string
      :reader b)))

(conspack:defencoding test-bodge
  a b)

(defmethod mp.clos-protocol:serialize (instance)
  (conspack:encode (conspack:encode-object instance)))

(defmethod mp.clos-protocol:deserialize ((instance simple-array))
  (conspack:decode instance))

(defmethod mp.clos-protocol:serialize ((instance number))
  (conspack:encode instance))

(defun bodge-equal (bodge1 bodge2)
  (and (string= (a bodge1) (a bodge2))
       (string= (b bodge1) (b bodge2))))

(parachute:define-test insert
  :parent moppack.test.clos-protocol

  (let* ((bodge1 (make-instance 'test-bodge :a "hello" :b "world"))
         (ref (mp.clos-protocol:insert bodge1))
         (bodge2 (mp.clos-protocol:find-instance ref)))
    (parachute:true (bodge-equal bodge1 bodge2))))
