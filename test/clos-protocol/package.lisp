#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.test.clos-protocol
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util)
                    (#:mp.page #:moppack.page)
                    (#:mp.table #:moppack.table)
                    (#:mp.reference #:moppack.reference)
                    (#:mp.clos-protocol #:moppack.clos-protocol))
  (:export
   #:moppack.test.clos-protocol))

(in-package #:moppack.test.clos-protocol)

(parachute:define-test moppack.test.clos-protocol
    :parent (#:moppack.test #:moppack.test))

