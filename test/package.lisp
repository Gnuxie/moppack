#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.test
  (:use #:cl)
  (:export
   #:moppack.test
   #:run
   #:ci-run
   #:configure-test))

(in-package #:moppack.test)

(parachute:define-test moppack.test)

(defun configure-test ()
  (moppack.table:initialize-pager)
  (setf moppack.table:*database-directory*
        (asdf:system-relative-pathname :moppack.test
                                       "test/.delme/"))
  (ensure-directories-exist moppack.table:*database-directory*))
(defun run (&key (report 'parachute:plain))
  (parachute:test 'moppack.test:moppack.test :report report))

(defun ci-run ()
  (configure-test)
  (let ((test-result (run)))
    (when (not (null (parachute:results-with-status :failed test-result)))
      (uiop:quit -1))))
