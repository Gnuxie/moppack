#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.test.table
  (:use #:cl)
  (:local-nicknames (#:mp.table #:moppack.table)
                    (#:mp.util #:moppack.util)
                    (#:mp.reference #:moppack.reference)
                    (#:mp.page #:moppack.page))
  (:export
   #:moppack.test.table))

(in-package #:moppack.test.table)

(parachute:define-test moppack.test.table
    :parent (#:moppack.test #:moppack.test))
