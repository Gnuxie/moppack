#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.test.table)

(parachute:define-test pager-blackbox

  ;;; what we're trying to do here is just check that these items will
  ;;; all be inserted into the same page.
  (let* ((table-ref (mp.table:next-table))
         (table (setf (mp.table:find-table table-ref)
                      (make-instance 'mp.table:table
                                     :page-class 'mp.page:variable-page
                                     :identifier table-ref
                                     :page-count 0))))
    (mp.table:table-push (conspack:encode "I don't really") table)
    (mp.table:table-push (conspack:encode "Care about these") table)
    (let* ((item "hello")
           (ref (mp.table:table-push (conspack:encode item) table)))
      (parachute:is #'string= item
                    (conspack:decode
                     (mp.table:find-item mp.table:*pager* ref))))
    (let ((file (mp.table:table-file table)))
      ;; atm we don't have a WAL log or anything (and I think that's what we're going for)
      ;; so this is how we're checking it, we should actually check this by introspecting though.
      (cacle:cache-flush (mp.table::pages mp.table::*pager*))
      (parachute:is #'= 4096 (trivial-file-size:file-size-in-octets file)
                    "We expect only 1 page was made, not 3."))))
