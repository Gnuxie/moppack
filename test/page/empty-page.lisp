#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.test.page)

(parachute:define-test empty-page
  :parent moppack.test.page

  (let ((page (make-instance 'mp.page:empty-page))
        (item1 (conspack:encode "hello"))
        (item2 (conspack:encode "world")))
    (let ((ref1 (mp.page:page-push item1 page))
          (ref2 (mp.page:page-push item2 page)))
      (parachute:is #'byte-array-equal item1
                    (mp.page:page-get page ref1 :length (length item1)))

      (parachute:is #'byte-array-equal item2
                    (mp.page:page-get page ref2 :length (length item2)))
      (let* ((saved-page
              (flexi-streams:with-output-to-sequence (s :element-type '(unsigned-byte 8))
                (mp.page:encode-page page s)))
             (loaded-page
              (flexi-streams:with-input-from-sequence (s saved-page)
                (mp.page:decode-page 'mp.page:empty-page s))))
        (parachute:is #'byte-array-equal item1
                      (mp.page:page-get loaded-page ref1 :length (length item1)))

        (parachute:is #'byte-array-equal item2
                      (mp.page:page-get loaded-page ref2 :length (length item2)))

        (parachute:is #'= (mp.page:pd-upper page)
                      (mp.page:pd-upper loaded-page))))))
