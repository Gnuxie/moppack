#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.test.page
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util)
                    (#:mp.page #:moppack.page))
  (:export #:moppack.test.page))

(in-package #:moppack.test.page)

(parachute:define-test moppack.test.page
    :parent (#:moppack.test #:moppack.test))
