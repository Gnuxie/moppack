#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.test.page)

(defun byte-array-equal (array1 array2)
  (declare (type (simple-array (unsigned-byte 8)) array1 array2))
  (when (= (length array1) (length array2))
    (loop :with equal? := t
       :for a :across array1
       :for b :across array2
       :unless (= a b) :do (return-from byte-array-equal nil)
       :finally (return-from byte-array-equal equal?))))

(parachute:define-test internals
  :parent moppack.test.page

  (let ((array (make-array 8 :element-type '(unsigned-byte 8)
                           :initial-element 0)))
    (mp.util:put-unsigned 4077 array 2 0)
    (mp.util:put-unsigned 4020 array 2 2)
    (parachute:is #'= 4077 (mp.util:get-unsigned array 2 0))
    (parachute:is #'= 4020 (mp.util:get-unsigned array 2 2))))

(parachute:define-test page-blackbox
  :parent moppack.test.page

  ;; basic blackbox test
  (let ((page (make-instance 'mp.page:variable-page :pd-upper 48 :pd-lower 4095))
        (item1 (conspack:encode "hello"))
        (item2 (conspack:encode "world")))
    (let* ((ref1 (mp.page:page-push item1 page))
           (ref2 (mp.page:page-push item2 page)))

      (parachute:is #'byte-array-equal item1
                    (mp.page:page-get page ref1))

      (parachute:is #'byte-array-equal item2
                    (mp.page:page-get page ref2))
      (let* ((saved-page
              (flexi-streams:with-output-to-sequence (s :element-type '(unsigned-byte 8))
                (mp.page:encode-page page s)))
             (loaded-page
              (flexi-streams:with-input-from-sequence (s saved-page)
                (mp.page:decode-page 'mp.page:variable-page s))))
        ;;; test that the page has saved properly.
        (parachute:is #'byte-array-equal item1
                      (mp.page:page-get loaded-page ref1))

        (parachute:is #'byte-array-equal item2
                      (mp.page:page-get loaded-page ref2))

        (parachute:is #'= (mp.page:pd-upper page)
                      (mp.page:pd-upper loaded-page))
        (parachute:is #'= (mp.page:pd-lower page)
                      (mp.page:pd-lower loaded-page))
        (parachute:is #'= (mp.page:item-count page)
                      (mp.page:item-count loaded-page))))))

