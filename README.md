# moppack

Persistence for CLOS using CRUD semantics.

The API is very unstable and this project is subject to change.

This is mostly being used to get into the headspace of the problems that we have to solve for object persistence in CL.

## License

NON-VIOLENT PUBLIC LICENSE v4+


Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>
