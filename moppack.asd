;;;; moppack.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:moppack
  :description "Persistence for clos using CRUD semantics"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "NON-VIOLENT PUBLIC LICENSE v4+"
  :version "0.0.1"
  :depends-on ("cl-conspack" "cl-ppcre" "alexandria" "cacle" "verbose")
  :serial t
  :components (
               (:module "code" :components
                        ((:module "util" :components
                                   ((:file "package")
                                    (:file "bytes")))
                         (:module "reference" :components
                                  ((:file "package")
                                   (:file "instance-pointer")))
                         (:module "page" :components
                                  ((:file "package")
                                   (:file "page")
                                   (:file "fixed-page")
                                   (:file "variable-page")
                                   (:file "empty-page")))
                         (:module "table" :components
                                  ((:file "package")
                                   (:file "table")
                                   (:file "pager")))
                         (:module "clos-protocol" :components
                                  ((:file "package")
                                   (:file "table-allocator")
                                   (:file "front-protocol")))))))
