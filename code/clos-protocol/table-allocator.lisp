#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.clos-protocol)

;;; the table allocator is the thing that takes a 'thing' a string, a class
;;; and provides a table for that thing. It does this by taking some kind
;;; identity from the 'thing' and you get the idea.
;;; all it's meant to do is provide a table for a class and by class we mean
;;; an instance of standard-class or otherwise, not a class-name
;;; although the naiive implementation is probably going to start with just
;;; class-name.
;;; mabe we need to provide per class-name identity too ie, you set the test function
;;; on a per class name basis if you want to.

(defvar *allocated-tables* (make-hash-table :test 'equal))
;;; this should be a weak hashtable!!!
(defvar *allocated-things* (make-hash-table))

(defun new-allocatable (allocatable)
  (let ((next-table (mp.table:next-table)))
    ;; this should be a weak hashtable but meh.
    (setf (gethash next-table *allocated-things*)
          allocatable)
    (setf (gethash allocatable *allocated-tables*)
          next-table)))

;;; could we make some kind of shortcut when we are following a btree
;;; if we say that the instances are read only
;;; a lot of querying archite.cc will do will be read only.
(defgeneric find-table-for-allocatable (allocatable)
  (:documentation ""))

(defmethod find-table-for-allocatable ((allocatable symbol))
  (let ((table-id (gethash allocatable *allocated-tables*)))
    (or table-id
        (new-allocatable allocatable))))

(defmethod find-table-for-allocatable ((allocatable string))
  (let ((table-id (gethash allocatable *allocated-tables*)))
    (or table-id
        (new-allocatable allocatable))))

(defun find-allocated-class (table-ref)
  (declare (type (unsigned-byte 16) table-ref))
  (gethash table-ref *allocated-things*))
