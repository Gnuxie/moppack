#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.clos-protocol
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util)
                    (#:mp.page #:moppack.page)
                    (#:mp.table #:moppack.table)
                    (#:mp.reference #:moppack.reference))
  (:export
   ;; protocol
   #:insert
   #:find-instance

   ;; serialization unfinalized
   #:serialize
   #:deserialize
   ))
