#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.clos-protocol)

(defgeneric insert (instance &key)
  (:documentation "stores the instance into the store and returns a reference to it."))

;;; for now we will provide identity only by the class-name
;;; later on we need to provide entity by lots of attributes on a class
;;; and we should provide this to the user as a test they can set.
(defmethod find-reference-table-for-instance-of-class (class)
  (let* ((instance-table-ref (find-table-for-allocatable (class-name class)))
         (instance-table (mp.table:find-table instance-table-ref)))
    (or instance-table
        (setf (mp.table:find-table instance-table-ref)
              (make-instance 'mp.table:table :page-class 'mp.page:fixed-page
                             :identifier instance-table-ref
                             :page-count 0
                             :page-initargs '(:item-width 9))))))

;;; we're going to have to thik of something better for find-table-for-allocatble
;;; since for this identity we technically want two tables.
(defmethod find-data-table-for-instance-of-class (class)
  (let* ((data-table-ref (find-table-for-allocatable
                     (concatenate 'string (symbol-name (class-name class))
                                  "-SLOT")))
         (data-table (mp.table:find-table data-table-ref)))
    (or data-table
        (setf (mp.table:find-table data-table-ref)
              (make-instance 'mp.table:table :page-class 'mp.page:variable-page
                             :identifier data-table-ref
                             :page-count 0)))))

(defmethod insert (instance &key &allow-other-keys)
  (let ((reference-table (find-reference-table-for-instance-of-class (class-of instance)))
        (slot-table (find-data-table-for-instance-of-class (class-of instance))))
    (let* ((slots-ref (mp.table:table-push (serialize instance) slot-table))
          (reference-ref (mp.table:table-push (serialize slots-ref) reference-table)))
      reference-ref)))

(defmethod find-instance ((ref integer))
  (declare (type (unsigned-byte 64) ref))
  (let ((instance-ref (deserialize (mp.table:find-item mp.table:*pager* ref))))
    (let ((class-name (find-allocated-class (mp.reference:table-id ref))))
      (conspack:decode-object class-name
                              (conspack:decode (mp.table:find-item mp.table:*pager* instance-ref))))))

;;; you could use test-serial or whatever it was called to create an alist to
;;; serialize something

;;; these are not decided yet, not really sure how we should serialize instances
;;; thre's room for optimization on things in terms of space too.
;;; we need to think more about this.

(defgeneric serialize (instance))
(defgeneric deserialize (instance))
