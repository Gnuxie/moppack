#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.table)

;;; the cache has cons ref . page
;;; this is because cacle doesn't give us the key when cleaning up
;;; and we need the key.

(defun provide-page (ref)
  (declare (type (unsigned-byte 48) ref))
  (values (cons ref
                (%get-page (mp.reference:table-id ref)
                           (mp.reference:page-number ref)))
          1))

(defun cleanup-page (ref-page-pair)
  (let ((ref (car ref-page-pair))
        (page (cdr ref-page-pair)))
    (%put-page page
               (mp.reference:table-id ref)
               (mp.reference:page-number ref))))

(defclass pager ()
  ((pages :initarg :pages
          :accessor pages
          :initform (cacle:make-cache 1250 #'provide-page :policy :lfuda
                                      :cleanup #'cleanup-page)
          :documentation "Cached pages.")))

(defvar *pager*)

(defun initialize-pager ()
  (setf *pager* (make-instance 'pager)))

;;; these should only be used by the pager to find pages for clients when they
;;; there is a cache miss.
(defmethod %get-page ((table-identifier integer) (page-number integer))
  (declare (type (unsigned-byte 16) table-identifier))
  (declare (type (unsigned-byte 32) page-number))
  (%get-page (find-table table-identifier) page-number))

(defmethod %get-page ((table table) (page-number integer))
  (declare (type (unsigned-byte 32) page-number))
  (if (page-exists-p table page-number)
      (with-open-file (s (table-file table) :direction :input
                         :element-type '(unsigned-byte 8)
                         :if-does-not-exist :create)
        (file-position s (* 4096 page-number))
        (let ((page (mp.page:decode-page (page-class table) s)))
          page))
      (prog1 (make-page (page-class table) table)
        (when (= page-number (page-count table))
          (incf (page-count table))))))

(defmethod %put-page (page table (page-number integer))
  (declare (type (unsigned-byte 32) page-number))
  (with-open-file (s (table-file table) :direction :output :if-exists :supersede
                     :if-does-not-exist :create
                     :element-type '(unsigned-byte 8))
    (file-position s (* 4096 page-number))
    (mp.page:encode-page page s)))
;;; maybe we could use a fancy enqueing thing that would queue reads/writes to the same
;;; table file and do them in order or file-position.

(defmethod find-page ((pager pager) (ref integer))
  (declare (type (unsigned-byte 64) ref))
  (cdr (cacle:with-cache-fetch page ((pages pager) (ldb (byte 48 0) ref))
         page)))

(defmethod find-item ((pager pager) (ref integer))
  (declare (type (unsigned-byte 64) ref))
  (mp.page:page-get (find-page pager ref)
                    (mp.reference:page-index ref)))

;;; my thinking is that we have some kind of transaction log until the
;;; page is taken out of cache and we know it's safe, then that part
;;; of the transaction log can be deleted.

;;; wait why can't you use a hashing function to skip part of the btree?
;;; ie you hash to where in the tree your thingo can be boiled down to
;;; a precise value
;;; basically a hashtable whos collision resolvation mechanism is a btree.

;;; is item required here? that would only be if you wanted to dispatch
;;; of it somehow, but since this method is not doing the pushing
;;; is it necessary?
;;; i think not
(declaim (inline find-page-for-insert))
(defun find-page-for-insert (table)
  (find-page-for-insert* table (page-class table)))

;;; FIXME 2020-03-08
;;; you need the page-exists thing here so that you can know whether to call
;;; next-page or whatever and incf the page-count.
;;; actually we need to find out who is incfing page-count tbh where
;;; is that happening?
;;; atm a new page is being created each time we call front-protcol::insert.
(defmethod find-page-for-insert* ((table table) page-class)
  (let* ((page-count (page-count table))
         (current-page (if (= 0 page-count)
                           0
                           (1- page-count)))
         (page-number (if (page-exists-p table current-page)
                          current-page
                          (page-count table))))
    (values
     (find-page *pager*
                (mp.reference:make-instance-pointer (identifier table) page-number 0))
     page-number)))

;;; don't forget you can GC the pages by moving but also scanning
;;; the pages for gaps in which page is being referenced, then the contents of
;;; that page can be moved to a new one to remove the gaps that would be there.
(defvar *table-count* 0)

;;; next table should be in here incase someone else wnats to write an allocator
;;; for some other thing that isn't clos.
(defun next-table ()
  (let ((next-table *table-count*))
    (assert (typep next-table '(unsigned-byte 32)))
    (incf *table-count*)
    next-table))

(defmethod table-push (item (table table))
  (multiple-value-bind (page-for-insert page-number)
      (find-page-for-insert table)
    (mp.reference:make-instance-pointer (identifier table)
                           page-number
                           (mp.page:page-push item page-for-insert))))

(defmethod table-push (item (table integer))
  (table-push item (find-table table)))

;;; how do you retrieve something thath as a reference to some other thing?
;;; well there are two ways to do thsi or something there's more but i'm just
;;; talking about these ones for now

;;; so you have a situtation where you have e.g. a room with a room-id and an
;;; event with an event-id
;;; to get from the event to the room you can register to say 'this slot 'event-id'
;;; means an index to one of thse things (a room)' then the thing can be replaced
;;; with a real reference, and when it's retrieved it can be replaced with the event-id
;;; this is useful because it means that you don't have to fuck aroudfn.

;;; but then the idea of room-id, even-id in place instead is just because
;;; the bad things can't represent references
;;; infact the refrences should only be replaced with event-id when they are
;;; serialized to json for federation (archite.cc not moppack)
;;; all this work was done in the name of not using a metaclass or having to wory
;;; about complicated cases but this is a complicated case.
;;; if you want to use live references and not an index like 'event-id'
;;; then you do need to use a metaclass.

;;; what a moppack reference would be like? well it'd be some kind of lazy loaded
;;; thingo mobobbo when you use an accessor it would go resolve the reference.
;;; the issue wit this is that what happens when you read from json and the json
;;; referes to an index, and you then want to use that like a live reference?
;;; well that will have to be worked out.

;;; basically there are two kinds of semantics when it comes to this.
;;; index semantics where references aren't real, they're just indexes
;;; this is how it looks in json atm

;;; then there's actual refreences.

;;; ok so after some gnuxie investiagtion that's not very good there's at least two kinds of semantics when it comes to references in sotres/databases
;;; 
;;;    1 'indexes' this is where you have something like an id that's ultimatum a string that gets thrown into your btree search/hashtable or anything and you end eventually up with a reference or an actual object. When you have your native objects that the programmer uses, they see these in their index representation and have to manually use them to get some other thing.
;;; 
;;;    2 actual references, this is where you decide that your native object has the slot bound to antoher native object (although you could be smart and do fancy lazy loading and things). Ultimatley the 1st option only really exists to avoid the complexity of the second . 2 has a problem though when you're operating on your graph in the native environment in that, if you fetch an object that has a reference chain like this a -> b -> c -> d... then you're fucked, how'd you handle that kind of thing? Some people make you say how much you want to come back bound e.g. "give me a and just b but not c". But then if you're not in the native environment this doesn't matter at all, you can traverse the references really quickly too. it makes a lot of sense to do mappings and other operations inside the implementation and not clos world (in the case of moppacks current design) and this is bad, it means something is wrong
;;; 
;;; in CLOSOS, the problems 2 has does not matter because the on disk/cached/paged/everything env is CLOS. So what'd you do? I said that i didn't want to make metaclasses but if you want to do things right with 2, you have to. Besides that, Moppack is great for the number 1 world
;;; what i would do for 2 with metaclasses is just shift the accessors to resolve references, and the slot-values themselves would be the references   

;;; just use something like a promise.
