#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.table)

;;; maybe intorudct a collection later on that can span multiple tables.
;;; probably isn't necessary, we have 2^32 pages remember?
;;; that's poages not bytes, it's the equivelent of 17TB
;;; you're not going to fill a table.

(defun initialize-tables ()
  (setf *tables* (make-hash-table)))

(defvar *tables* (initialize-tables) "This isn't how we're going to find tables
but it'll do for now.")
(defvar *database-directory*)

(defclass table ()
  ((page-class :initarg :page-class
               :accessor page-class
               :type symbol
               :documentation "Symbol naming a class")

   (identifier :initarg :identifier
               :accessor identifier
               :type (unsigned-byte 16)
               :documentation "The identifire for this table.")

   (page-count :initarg :page-count
               :accessor page-count
               :initform 0
               :type (unsigned-byte 32)
               :documentation "The number of pages that have been allocated
in the table.")

   (page-initargs :initarg :page-initargs
                  :accessor page-initargs
                  :initform '()
                  :type list
                  :documentation "When the table needs to create a new page
these will be used to override the default initargs used by #'make-page.")))

(defmethod page-exists-p ((table table) (page-number integer))
  (declare (type (unsigned-byte 32) page-number))
  (< page-number (page-count table)))

(defun find-table (identifier)
  (declare (type (unsigned-byte 16) identifier))
  (gethash identifier *tables*))

(defun set-find-table (identifier new-table)
  (declare (type (unsigned-byte 16) identifier))
  (declare (type table new-table))
  (setf (gethash identifier *tables*) new-table))

(defsetf find-table set-find-table)

(defmethod table-file ((table table))
  (table-file (identifier table)))

(defmethod table-file ((table integer))
  (merge-pathnames *database-directory*
                   (princ-to-string table)))

(defgeneric make-page (class-name table)
  (:documentation "Make a new page, usually called by the pager."))

(defmethod make-page ((class-name (eql 'mp.page:fixed-page)) (table table))
  (apply #'make-instance 'mp.page:fixed-page
         (list* :data (make-array 4096 :element-type '(unsigned-byte 8)
                                  :initial-element 0)
                (page-initargs table))))

(defmethod make-page ((class-name (eql 'mp.page:variable-page)) (table table))
  (let ((new-page
         (make-instance 'mp.page:variable-page :pd-upper 48
                        :data (make-array 4096 :element-type '(unsigned-byte 8)
                                          :initial-element 0))))
    (setf (mp.page:pd-lower new-page)
          (1- (length (mp.page:data new-page))))
    new-page))

;;; variable-page needs changing not to store the length of the thing.
;;; any meta like that should be stored in the instance-pointer.
;;; that way, we can read across several pages and fragmented pages too.
;;; therefor the variable-page should have no concept of item length in itself.
;;; that's for the reader to figure out.
;;;
;;; if the length has been moved to the instance-pointer then we can also use
;;; 2^12 on the instance pointer for the length (4096)
;;; and 2^4 for flags!!!
;;; one flag will have to be whether the next instance pointer has the next
;;; block.
