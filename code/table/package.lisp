#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.table
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util)
                    (#:mp.page #:moppack.page)
                    (#:mp.reference #:moppack.reference))
  (:export
   ;; table
   #:*database-directory*
   #:table
   #:page-class
   #:identifier
   #:page-count
   #:page-initargs
   #:page-exists-p
   #:find-table
   #:find-page
   #:table-file
   #:make-page
   ;; pager
   #:initialize-pager
   #:*pager*
   #:next-table
   #:find-table
   #:find-page-for-insert
   #:find-page-for-insert*
   #:table-push
   #:find-item))
