#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

;;; could use a define-page macro that determines the total header size for us
;;; using mop.

;;; remember we also want to be able to partition instances on multiple btrees
;;; e.g. first you traverse the room-id btree, then an event-id btree
;;; to reduce the load on looking for an event.
;;; this ofc means that looking for an event-id without a room-id is very hard
;;; but in the matrix spec this only happens on deprecated events.
(defclass b+tree (standard-page)
  ((record-size :initarg :record-size
                :accessor record-size
                :type (unsigned-byte 8)
                :documentation "The size of a record to read")

   (branching-factor :initarg :branching-factor
                     :accessor branching-factor
                     :type (unsigned-byte 8))

   (key-size :initarg :key-size
             :accessor key-size
             :type (unsigned-byte 8))))

;;; we can use an (unsigned-byte 2) to say what the thing is
;;; 00 for root 01 for internal 10 for leaf 11 for record.
;;; then (unsigned-byte 1) for an internal or external instance poitner
;;; internal meaning, it's in this page. I guess actually this could be useless
;;; since we would have to allocate the same amount of space incaes things change
;;; unless we find somewhere special to put external ones.
;;; all external ones will just be put at the top
;;; tree grows from the bottom.
;;; each pointer is u2, even external (to go to the top and resolve it)
;;; [tag|pointer-type][instance-pointer]
;;; actually all of this is rubbish because internal nodes only have links to
;;; other nodes and no records so it's safe to assume them as usngined-byte 2
;;; naa they need to be the same size becaues they move, not because we can't
;;; tell if they're one or the other.

(defmethod optimal-branching-factor ((tree b+tree))
  (1- (floor (key-size tree) (free-space tree))))

(defmethod shared-initialize :after ((tree b+tree) slot-names &rest initargs
                                     &key   &allow-other-keys)
  
  (let ((branching-factor
         (getf initargs :branching-factor (optimal-branching-factor tree))))
    (setf (branching-factor tree)
          branching-factor))
  tree)
;;; [tag][u2-internal-pointer][key][pointer], or do lambda over let over lambda
(defclass b+tree-walker ()
  ((current-key-index :initarg :current-key-index
                       :accessor current-key-index
                       :type (unsigned-byte 16)
                       :documentation "Maybe we don't make the walker page specific
and we keep this as the full reference.
The index of the key")

   (current-key-number :initarg :current-key-number
                       :accessor current-key-number
                       :type (unsigned-byte 8)
                       :documentation "like 0-b or something"
                       )
   (node-index :initarg :node-index
               :accessor node-index
               :type (unsigned-byte 16)
               :documentation "The index of the node within the page.")
   (%operating-page :initarg :operating-page
                    :accessor operating-page
                    :type b+tree
                    :documentation "The current age.")

   (%traversal-chain :initarg :traversal-chain
                     :accessor traversal-chain
                     :type list
                     :documentation "For backtracking parents")))

(defmethod %node-type ((tree b+tree) (node-index integer))
  (declare (type (unsigned-byte 16) node-index))
  ()
  ())

(defmethod left ((walker b+tree-walker) (node-type (eql :internal)))
  (get-unsigned (data (%operating-page walker)) 2 (- (current-key-index walker) 2)))
(defmethod right ((walker b+tree-walker) (node-type (eql :internal)))
  (get-unsigned (data (%operating-page walker)) 2 (+ (current-key-index walker) 2)))

(defmethod left ((walker b+tree-walker) (node-type (eql :leaf)))
  (- (current-key-index walker)
     (record-size (%operating-page walker))))

(defmethod right ((walker b+tree-walker) (node-type (eql :leaf)))
  (+ (current-key-index walker)
     (key-size (%operating-page walker))))

(defmethod next-key-index ((walker b+tree-walker) (node-type (eql :internal)))
  (let ((page (%operating-page walker)))
    (when (> (branching-factor page) (1+ (current-key-number walker)))
      (+ (current-key-index walker)
         (+ (key-size page) 2)))))
(defmethod next-key-index ((walker b+tree-walker) (node-type (eql :leaf)))
  (let ((page (%operating-page walker)))
    (assert (> (branching-factor page) (1+ (current-key-number walker))))
    (+ (current-key-index walker)
       (+ (key-size page) (record-size page)))))

(defmethod key< ((tree b+tree) key ref))

(defmethod key> ((tree b+tree) key ref))

(defmethod search ((tree b+tree) key)
  (tree-search tree (make-btree-walker) key))
(defmethod tree-search ((tree b+tree) (walker b+tree-walker)
                        (node-type (eql :internal)) key)
  (cond ((key< tree key (current-key-index walker))
         )))

;;; we should index with hash tables for now until we figure out
;;; paging mechanics.



