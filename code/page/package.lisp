#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.page
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util))
  (:export
   ;; page.lisp
   #:load-page-data
   #:page
   #:page-push
   #:page-get
   #:free-space
   #:decode-page
   #:encode-page
   #:standard-page
   #:item-count
   #:data
   #:total-header-size
   ;; fixed-page
   #:item-width
   #:fixed-page
   ;; variable-page
   #:variable-page
   #:pd-upper
   #:pd-lower
   ;; standard-page
   #:empty-page
   ))
