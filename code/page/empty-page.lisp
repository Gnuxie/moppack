#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.page)

;;; this is a page for dumping things, does not attempt to track length
;;; does not track anything but the allocated space

(defclass empty-page (page)
  ((pd-upper :initarg :pd-upper
             :accessor pd-upper
             :initform 2
             :type (unsigned-byte 16)
             :documentation "keep track of free space.")

   (data :initarg :data
         :accessor data
         :initform (make-array 4096 :element-type '(unsigned-byte 8) :initial-element 0)
         :type (simple-array (unsigned-byte 8)))

   (total-header-size :reader total-header-size
                      :initform 16
                      :allocation :class
                      :type (unsigned-byte 16))))

(defmethod decode-page ((class-name (eql 'empty-page)) stream &key &allow-other-keys)
  (let ((data (load-page-data stream 4096)))
    (let ((pd-upper (mp.util:get-unsigned data 2 0)))
      (make-instance 'empty-page
                     :pd-upper pd-upper
                     :data data))))

(defmethod encode-page ((page empty-page) stream &key &allow-other-keys)
  (mp.util:put-unsigned (pd-upper page) (data page) 2 0)
  (write-sequence (data page) stream))

(defmethod page-get ((page empty-page) ref &key length)
  (assert (not (null length)))
  ;;; TODO there has to be a better way of getting the length here, this type
  ;;; of page needs it but the others don't.
  (subseq (data page) ref
          (+ ref length)))

(defmethod free-space ((page empty-page))
  (- 4096 (pd-upper page)))

(defmethod page-push (item (page empty-page))
  (declare (type (simple-array (unsigned-byte 8)) item))
  (with-accessors ((pd-upper pd-upper) (data data)) page
    (assert (> (free-space page) (length item)))
    (let ((new-pd-upper (+ pd-upper (length item))))
      (setf (subseq data pd-upper new-pd-upper)
            item)
      (prog1 pd-upper
        (setf pd-upper new-pd-upper)))))
