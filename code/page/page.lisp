#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.page)

(defun load-page-data (stream size)
  (let ((data (make-array size :element-type '(unsigned-byte 8))))
    (read-sequence data stream)
    data))

(defclass page () ())

(defgeneric page-push (item place)
  (:documentation "Add this item to the page.
item, place ==> ref"))

(defgeneric page-get (place ref &key)
  (:documentation "Get the item at the reference from the page."))

(defgeneric free-space (page)
  (:documentation "The amount of free space available for just one item
Taking into account any metadata that the page would have to store if it
accepted a new item."))

(defgeneric decode-page (class-name stream &key)
  (:documentation "create an instance from the stream."))

(defgeneric encode-page (page stream &key)
  (:documentation "encode the page to the stream"))

;;; i really think the table-ref and page-ref should be removed too.
;;; since these are only used by the allocator and it should isntead use
;;; cons ref page if it's really necessary.

(defclass standard-page (page)
  ((item-count :initarg :item-count
               :accessor item-count
               :initform 0
               :type (unsigned-byte 16)
               :documentation "The number of items in the page")

   (data :initarg :data
         :accessor data
         :initform (make-array 4096 :element-type '(unsigned-byte 8) :initial-element 0)
         :type (simple-array (unsigned-byte 8)))

   (total-header-size :reader total-header-size
                      :initform 16
                      :type (unsigned-byte 16)
                      :allocation :class
                      :documentation "The total size of the header")))


