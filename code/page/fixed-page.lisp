#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.page)
;;; this is a page with fixed-width items
;;; usually references to slots.

;;; we can save some time by just saying that you give the unsigned-byte 16
;;; reference without it being normalised e.g. the third item might be 48 instead
;;; of 3.

;;; this is something we could do in the variable width table too
;;; and remove the need for 'index' rows at the top.
(defclass fixed-page (standard-page)
  ((item-width :initarg :item-width
               :reader item-width
               :initform (error "Must supply the width of a fixed-page")
               :type (unsigned-byte 8)
               :documentation "The width of all items in the page")

   (total-header-size :initform 24
                      :allocation :class
                      :type (unsigned-byte 16))))

(defmethod free-space ((page fixed-page))
  (- (length (data page))
     (total-header-size page)
     (* (item-width page)
        (item-count page))))

(defmethod page-get ((page fixed-page) (ref integer) &key)
  (declare (type (unsigned-byte 16) ref))
  (subseq (data page) ref (+ ref (item-width page))))

(defmethod (setf page-get) ((item simple-array)
                            (page fixed-page) (ref integer))
  (declare (type (simple-array (unsigned-byte 8)) item))
  (declare (type (unsigned-byte 16) ref))
  (assert (>= (item-width page) (length item)))
  (let ((end (+ ref (item-width page))))
    (setf (subseq (data page) ref end)
          item)))

(defmethod page-push ((item simple-array) (page fixed-page))
  (declare (type (simple-array (unsigned-byte 8)) item))
  (let ((next-ref (+ (total-header-size page)
                     (* (item-width page) (item-count page)))))
    (setf (page-get page next-ref) item)
    (prog1 next-ref
      (incf (item-count page)))))

;;; tables need initalisation arguments that can be given to
;;; make page as overrides or something.

(defmethod decode-page ((class-name (eql 'fixed-page)) stream &key &allow-other-keys)
  (let ((data (load-page-data stream 4096)))
    (let ((item-count (mp.util:get-unsigned data 2 0))
          (item-width (mp.util:get-unsigned data 1 2)))
      (make-instance 'fixed-page
                     :item-count item-count
                     :item-width item-width
                     :data data))))

(defmethod encode-page ((page fixed-page) stream &key &allow-other-keys)
  (mp.util:put-unsigned (item-count page) (data page) 2 0)
  (mp.util:put-unsigned (item-width page) (data page) 1 2)
  (write-sequence (data page) stream))
