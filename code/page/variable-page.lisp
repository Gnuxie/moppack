#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.page)

(defclass variable-page (standard-page)
  ((pd-upper :initarg :pd-upper
             :accessor pd-upper
             :type (unsigned-byte 16)
             :documentation "This is the offset to the end of the free space
for items, it's assumed that the start is the end of the page.

It's basically a devidor between where to write id's and items.

See https://www.postgresql.org/docs/current/storage-page-layout.html

Indexes|
items ¬, index points to start.
|start end|start end|")

   (pd-lower :initarg :pd-lower
             :accessor pd-lower
             :type (unsigned-byte 16)
             :documentation "The lower is needed to show where the items end
the upper is needed to show where the indexes end.")
   (total-header-size :initform 48
                      :allocation :class
                      :type (unsigned-byte 16))))

(defmethod decode-page ((class-name (eql 'variable-page)) stream &key &allow-other-keys)
  (let ((data (load-page-data stream 4096)))
    (let ((item-count (mp.util:get-unsigned data 2 0))
          (pd-upper (mp.util:get-unsigned data 2 2))
          (pd-lower (mp.util:get-unsigned data 2 4)))
      (make-instance 'variable-page
                     :item-count item-count
                     :pd-upper pd-upper
                     :pd-lower pd-lower
                     :data data))))

(defmethod encode-page ((page variable-page) stream &key &allow-other-keys)
  (mp.util:put-unsigned (item-count page) (data page) 2 0)
  (mp.util:put-unsigned (pd-upper page) (data page) 2 2)
  (mp.util:put-unsigned (pd-lower page) (data page) 2 4)
  (write-sequence (data page) stream))

(defvar *page-total-size* 4096
  "gah you're going to have to look for this and hardocded values and take away
the size of the headers from it.")

;;; ok we're just going to say each item has a single header saying the offset
;;; to where it ends.
;;; this kind of limits you to how big an item can be though but
;;; tbh why are we even worrying.
;;; postgres puts this with the index though.
(defmethod page-get ((page variable-page) ref &key)
  (let ((item-offset
          (mp.util:get-unsigned (data page) 1 ref)))
    (subseq (data page) (1+ ref)
            (+ ref 1 item-offset))))
;;; honestly don't even try to do these (1+ ...)bs intermediate stages
;;; put them in let*, it makes what's happening so much clearer.

(defmethod free-space ((page variable-page))
  (- (pd-lower page) (pd-upper page) (total-header-size page)))

(defmethod page-push (item (page variable-page))
  (declare (type (simple-array (unsigned-byte 8)) item))
  (with-accessors ((pd-lower pd-lower) (pd-upper pd-upper) (data data)) page
    (assert (> (free-space page) (length item)))
    (let* ((new-pd-lower (- pd-lower (length item) 1))
           (offset-index (1+ new-pd-lower))
           (item-index (1+ offset-index)))
      (setf (subseq (data page) item-index (1+ pd-lower))
            item)
      ;; add offest to the end of the item.
      (setf (aref (data page) offset-index)
            (length item))
      (setf (pd-lower page) new-pd-lower)
      (1+ pd-lower))))
