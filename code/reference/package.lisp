#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.reference
  (:use #:cl)
  (:local-nicknames (#:mp.util #:moppack.util))
  (:export
   #:destructure-instance-pointer
   #:table-id
   #:page-number
   #:page-index
   #:make-instance-pointer))

(in-package #:moppack.reference)
