#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.reference)

(defmacro with-u-reader ((array-var offset-var) &body body)
  `(lambda (,array-var &optional (,offset-var 0))
     (declare (type (simple-array (unsigned-byte 8)) ,array-var))
     ,@body))

(defun make-instance-pointer-handlers (identifier-size page-number-size page-index-size)
  "size is the number of (unsigned-byte 8)s"
  (declare (type integer identifier-size page-number-size page-index-size))
  (let ((identifier-index 0)
        (page-number-index identifier-size)
        (page-index-index (+ identifier-size page-number-size))
        (total-size (+ identifier-size page-number-size page-index-size)))
    (values
     ;; read table identifier
     ;; read page number
     ;; read index
     ;; writer for the whole thing.
     (with-u-reader (array offset)
       (mp.util:get-unsigned array identifier-size (+ offset identifier-index)))
     (with-u-reader (array offset)
       (mp.util:get-unsigned array page-number-size (+ offset page-number-index)))
     (with-u-reader (array offset)
       (mp.util:get-unsigned array page-index-size (+ offset page-index-index)))
     (lambda (table-identifier page-number page-index
              &key (array (make-array total-size :element-type '(unsigned-byte 8)
                                      :initial-element 0))
                (offset 0))
       (mp.util:put-unsigned
        table-identifier array identifier-size  (+ offset identifier-index))
       (mp.util:put-unsigned
        page-number  array page-number-size (+ offset page-number-index))
       (mp.util:put-unsigned
        page-index  array page-index-size (+ offset page-index-index))
       array))))

;;; we can then add the tag for the instance in the page it is stored in
;;; rather than on each pionter.
;;; then we can just shift the reference when the instance is upgraded.

(defmacro with-instance-pointer-handlers ((identifier-var iv-size)
                                          (page-number-var pnv-size)
                                          (page-index-var piv-size)
                                          writer-var
                                          &body body)
  `(multiple-value-bind (,identifier-var ,page-number-var ,page-index-var ,writer-var)
       (make-instance-pointer-handlers ,iv-size ,pnv-size ,piv-size)
     ,@body))

(defmacro with-instance-pointer-handlers* ((identifier-var iv-size)
                                           (page-number-var pnv-size)
                                           (page-index-var piv-size)
                                           writer-var
                                           &body body)
  (alexandria:with-gensyms (id-fun page-number-fun page-index-fun writer-fun)
    `(with-instance-pointer-handlers (,id-fun ,iv-size)
         (,page-number-fun ,pnv-size)
         (,page-index-fun ,piv-size)
         ,writer-fun
       (macrolet ((,identifier-var (&rest args)
                    (list* 'funcall ',id-fun args))
                  (,page-number-var (&rest args)
                    (list* 'funcall ',page-number-fun args))
                  (,page-index-var (&rest args)
                    (list* 'funcall ',page-index-fun args))
                  (,writer-var (&rest args)
                    (list* 'funcall ',writer-fun args)))
         ,@body))))

(declaim (inline destructure-instance-pointer))
(defun destructure-instance-pointer (ref)
  (declare (type (unsigned-byte 64) ref))
  (values (ldb (byte 16 0) ref)
          (ldb (byte 32 16) ref)
          (ldb (byte 16 48) ref)))

(declaim (inline table-id))
(defun table-id (ref)
  (declare (type (unsigned-byte 64) ref))
  (ldb (byte 16 0) ref))

(declaim (inline page-number))
(defun page-number (ref)
  (declare (type (unsigned-byte 64) ref))
  (ldb (byte 32 16) ref))

(declaim (inline page-index))
(defun page-index (ref)
  (declare (type (unsigned-byte 64) ref))
  (ldb (byte 16 48) ref))

(defun make-instance-pointer (table-id page-number page-ref)
  (declare (type (unsigned-byte 16) table-id page-ref))
  (declare (type (unsigned-byte 32) page-number))
  (let ((tmp table-id))
    (declare (type (unsigned-byte 64) tmp))
    (setf (ldb (byte 32 16) tmp) page-number)
    (setf (ldb (byte 16 48) tmp) page-ref)
    tmp))
