#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:moppack.util)

(defun number-from-bytes (&rest bytes)
  "little endian"
  (loop :with result := 0
     :for byte :in bytes
     :for count :from 0 :below (length bytes)
     :do (setf (ldb (byte 8 (ash count 3)) result)
               byte)
       :finally (return-from number-from-bytes result)))

(defun get-unsigned (array size index)
  "little endian"
  (loop :with result := 0
     :for i :from 0 :below size
     :do (setf (ldb (byte 8 (ash i 3)) result)
               (aref array (+ index i)))
     :finally (return-from get-unsigned result)))

(defun put-unsigned (item array size index)
  "little endian"
  (loop :for i :from 0 :below size
     :do (setf (aref array (+ index i))
               (ldb (byte 8 (ash i 3)) item)))
  array)

(defun read-unsigned (stream size)
  (loop :with result := 0
     :for i :from 0 :below size
     :do (setf (ldb (byte 8 (ash i 3)) result)
               (read-byte stream))
     :finally (return-from read-unsigned result)))
