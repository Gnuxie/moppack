#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:moppack.util
  (:use #:cl)
  (:export
   #:number-from-bytes
   #:get-unsigned
   #:put-unsigned
   #:read-unsigned))

(in-package #:moppack.util)
