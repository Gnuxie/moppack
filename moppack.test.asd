(asdf:defsystem #:moppack.test
  :description "Tests for moppack"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "NON-VIOLENT PUBLIC LICENSE v4+"
  :version "0.0.1"
  :depends-on ("moppack" "parachute" "trivial-file-size" "flexi-streams")
  :serial t
  :components ((:module "test" :components
                        ((:file "package")
                         (:module "page" :components
                                  ((:file "package")
                                   (:file "page")
                                   (:file "empty-page")))
                         (:module "clos-protocol" :components
                                  ((:file "package")
                                   (:file "front-protocol")))))))
